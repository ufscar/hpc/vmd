#==============================================================================
# Authors:   Paulo Matias (at ufscar.br)
#            Alisue (lambdalisue@hashnote.net)
#            Al42and (al42and@gmail.com)
# License:   MIT License
#
# Copyright hashnote.net, 2015, allright reserved.
# Copyright UFSCar, 2020
#
# Based on: https://gist.github.com/al42and/2c1350191ab273e0777277d12f4bef21
#==============================================================================

Bootstrap: docker
From: nvidia/cudagl:10.2-devel
Stage: build

%files
  cuda-backport.diff /root

%post
  VERSION="1.9.3"
  ARCH="LINUXAMD64"
  VMDINSTALLNAME="vmd"
  VMDINSTALLBINDIR="/opt/$VMDINSTALLNAME/bin"
  VMDINSTALLLIBRARYDIR="/opt/$VMDINSTALLNAME/lib"
  TCL_INCLUDE_DIR="/usr/include/tcl8.6"
  PYTHON_INCLUDE_DIR="/usr/include/python2.7"

  export DEBIAN_FRONTEND=noninteractive
  apt-get update
  apt-get install -y wget unzip ed tk-dev libfltk1.3-dev xutils-dev libnetcdf-dev python-dev python-numpy

  ROOT=/root
  cd $ROOT

  wget https://developer.download.nvidia.com/compute/redist/VideoCodec/v8.2/NvCodec.zip
  unzip -j NvCodec.zip \
    NvCodec/NvDecoder/cuviddec.h \
    NvCodec/NvDecoder/nvcuvid.h \
    NvCodec/NvEncoder/nvEncodeAPI.h \
    -d /usr/local/cuda/include
  unzip -j NvCodec.zip \
    NvCodec/Lib/linux/stubs/x86_64/libnvcuvid.so \
    NvCodec/Lib/linux/stubs/x86_64/libnvidia-encode.so \
    -d /usr/local/cuda/lib64/stubs

  wget https://www.ks.uiuc.edu/Research/vmd/vmd-${VERSION}/files/final/vmd-${VERSION}.src.tar.gz
  tar xvzf "vmd-${VERSION}.src.tar.gz"

  echo "************************************************************************"
  echo
  echo "Compile VMD plugins..."
  echo
  echo "************************************************************************"
  cd ${ROOT}/plugins
  sed -i -e 's/-ltcl8.5/-ltcl8.6/g' ./Make-arch
  make clean
  make $ARCH TCLINC=-I$TCL_INCLUDE_DIR TCLLIB=-F/usr/lib
  make distrib PLUGINDIR=${ROOT}/vmd-${VERSION}/plugins

  echo "************************************************************************"
  echo
  echo "Compile STRIDE..."
  echo
  echo "************************************************************************"
  cd ${ROOT}/vmd-${VERSION}/lib/stride
  mkdir src
  cd src
  wget 'http://webclu.bio.wzw.tum.de/stride/stride.tar.gz'
  tar zxf stride.tar.gz
  sed -i -e 's/\(MAX_AT_IN_RES \+\)50/\175/' ./stride.h
  sed -i -e 's/SUCCESS/0/' ./stride.c
  make
  cp stride ../stride_$ARCH

  echo "************************************************************************"
  echo
  echo "Compile SURF..."
  echo
  echo "************************************************************************"
  cd ${ROOT}/vmd-${VERSION}/lib/surf
  mkdir src
  cd src
  tar axf ../surf.tar.Z
  make depend
  make
  cp surf ../surf_$ARCH

  echo "************************************************************************"
  echo
  echo "Compile TACHYON..."
  echo
  echo "************************************************************************"
  cd ${ROOT}/vmd-${VERSION}/lib
  wget 'http://jedi.ks.uiuc.edu/~johns/raytracer/files/0.99b6/tachyon-0.99b6.tar.gz'
  tar zxf tachyon-0.99b6.tar.gz
  cd tachyon/unix
  make linux-64-thr
  cp ../compile/linux-64-thr/tachyon ../tachyon_$ARCH

  echo "************************************************************************"
  echo
  echo "Compile VMD..."
  echo
  echo "************************************************************************"
  export VMDINSTALLNAME
  export VMDINSTALLBINDIR
  export VMDINSTALLLIBRARYDIR
  export TCL_INCLUDE_DIR
  export PYTHON_INCLUDE_DIR

  cd ${ROOT}/vmd-${VERSION}
  ln -sf ${ROOT}/plugins plugins
  patch -Np1 -i /root/cuda-backport.diff

  # Create modified configure file
  cp ./configure ./configure.mod
  sed -i -e 's/-lm"/-lm -lrt"/' ./configure.mod
  sed -i -e 's/-ltk8.5/-ltk8.6/g' ./configure.mod
  sed -i -e 's/-ltcl8.5/-ltcl8.6/g' ./configure.mod
  sed -i -e 's/-lpython2\.5/-lpython2.7/g' ./configure.mod
  sed -i -e 's/cuda-8.0/cuda-10.2/g' ./configure.mod
  sed -i -e '/compute_1[03]/d' ./configure.mod
  sed -i -e "s/\(\$arch_nvccflags[ ]\+= \"\)/\1-D_FORCE_INLINES /" ./configure.mod
  sed -i -e "s/\$libtachyon_dir\/include/\$libtachyon_dir\/src/" ./configure.mod
  sed -i -e "s/\$libtachyon_dir\/lib_\$config_arch/\$libtachyon_dir\/compile\/linux-64-thr/" ./configure.mod
  sed -i -e "s/^\$nvenc_include.*/\$nvenc_include = \"-I\/usr\/local\/cuda\/include\";/" configure.mod
  sed -i -e "s/^\$nvenc_library.*/\$nvenc_library = \"-L\/usr\/local\/cuda\/lib64\/stubs\";/" configure.mod
  chmod +x ./configure.mod

  echo "Configuring"
  ./configure.mod \
      LINUXAMD64 \
      FLTKOPENGL \
      EGLPBUFFER \
      NVENC \
      FLTK \
      TK \
      IMD \
      SILENT \
      TCL \
      PTHREADS \
      NETCDF \
      PYTHON \
      NUMPY \
      CUDA \
      LIBTACHYON

  # Compile
  cd src
  make veryclean
  make
  make install


Bootstrap: docker
From: nvidia/cudagl:10.2-runtime
Stage: final

%files
  xorg.conf.nvidia-headless /etc/X11/xorg.conf
%files from build
  /usr/local/cuda/lib64/stubs/libnvcuvid.so /usr/local/cuda/lib64/stubs/libnvcuvid.so
  /usr/local/cuda/lib64/stubs/libnvidia-encode.so /usr/local/cuda/lib64/stubs/libnvidia-encode.so
  /opt/vmd /opt/vmd

%post
  export DEBIAN_FRONTEND=noninteractive
  apt-get update
  apt-get install -y \
    xserver-xorg-core libxv1 x11-xserver-utils xserver-xorg-video-nvidia-440 \
    xauth xkb-data x11-xkb-utils openbox \
    wget rlwrap libopengl0 libglu1-mesa \
    tk libfltk-gl1.3 libnetcdf13 libpython2.7 python python-numpy
  cd /tmp
  wget https://ufpr.dl.sourceforge.net/project/virtualgl/2.6.3/virtualgl_2.6.3_amd64.deb
  wget https://ufpr.dl.sourceforge.net/project/turbovnc/2.2.5/turbovnc_2.2.5_amd64.deb
  dpkg -i virtualgl_2.6.3_amd64.deb turbovnc_2.2.5_amd64.deb
  apt-get install -f
  rm /tmp/*.deb
  rm -rf /var/lib/apt/lists/*

%runscript
  exec /opt/vmd/bin/vmd "$@"
